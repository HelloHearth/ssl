
'use strict';

$(window).ready(function() {

    // Hide the sb-admin-2 sidebar with small device onLoad
    if ($(window).width() < 992) {
        $('body').addClass('sidebar-toggled');
        $('.sidebar').addClass('toggled');
        $('.sidebar .collapse').collapse('hide');
    }

    $('.js-hide-video').on('submit', function (e) {

        e.preventDefault();
        let $form = $(this);
        const $youtubeVideo = $(e.target).closest('.youtube-video');
        const $youtubeChannel = $youtubeVideo.closest('.youtube-channel');
        const $hiddenVideosList = $('#youtube-channels-footer .youtube-hidden-channels');
        $youtubeVideo.find('.alert-error').remove();

        $.ajax({
            method: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
        }).done(function (data) {
            $(e.target).closest('.youtube-video').hide(250, function () {
                $youtubeVideo.remove();
                if ($youtubeChannel.length > 0 && $youtubeChannel.find('.youtube-video').length === 0) {
                    $youtubeChannel.remove();
                }
            });
            $hiddenVideosList.prepend('<li>'+data+'</li>');
        }).fail(function (jqXHR) {
            $youtubeVideo.append('<p class="alert-error">'+ jqXHR.responseText +'</p>');
        });
    })
});
