<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201107164427 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE EmailAliasClassic (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(512) NOT NULL, forward VARCHAR(1024) NOT NULL, comment LONGTEXT DEFAULT NULL, isEnabled TINYINT(1) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_49082EDDE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE EmailAliasFilter (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(512) NOT NULL, forward VARCHAR(1024) NOT NULL, comment LONGTEXT DEFAULT NULL, isEnabled TINYINT(1) NOT NULL, fromURL VARCHAR(4000) DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_97F64C03E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE EmailMaildir (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(512) NOT NULL, comment LONGTEXT DEFAULT NULL, password VARCHAR(1024) NOT NULL, isEnabled TINYINT(1) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_47B80636E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE EmailAliasClassic');
        $this->addSql('DROP TABLE EmailAliasFilter');
        $this->addSql('DROP TABLE EmailMaildir');
    }
}
