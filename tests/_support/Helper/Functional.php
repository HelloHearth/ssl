<?php

declare(strict_types=1);

namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Codeception\TestInterface;
use donatj\MockWebServer\MockWebServer;

class Functional extends \Codeception\Module
{
    /** @var MockWebServer */
    private static $mockedWebServer;

    /*
     * Workaround:
     *
     * Don't print the response from the REST module in the body artefact of
     * codeception, it can output a very long text...
     *
     * See: https://github.com/Codeception/Codeception/issues/5826
     */
    public function _failed(TestInterface $test, $fail): void
    {
        $reports = $test->getMetadata()->getReports();
        $shortResponseBody = $this->_getConfig('shortResponseBody');

        if (! isset($reports['body'], $shortResponseBody)) {
            return;
        }

        if (in_array($shortResponseBody, range(0, 9))) {
            $test->getMetadata()->addReport('body', '');
            return;
        }

        if ($shortResponseBody > 9) {

            $divided = (int) ceil($shortResponseBody / 2);

            $start = substr($reports['body'], 0, $divided);
            $end = substr($reports['body'], -$divided);
            $newBody = $start . ' (...) ' . $end;

            $test->getMetadata()->addReport('body', $newBody);
        }
    }

    public function _afterSuite()
    {
        if (self::$mockedWebServer instanceof MockWebServer && self::$mockedWebServer->isRunning()) {
            self::$mockedWebServer->stop();
        }
    }

    public static function startMockWebserver()
    {
        if (! self::$mockedWebServer instanceof MockWebServer || ! self::$mockedWebServer->isRunning()) {

            $server = new MockWebServer(7050);
            $server->start();

            self::$mockedWebServer = $server;
        }
    }

    public static function getMockWebserverInstance(): ?MockWebServer
    {
        return self::$mockedWebServer;
    }
}
