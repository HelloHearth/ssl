<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller;

use App\Tests\FunctionalTester;

class DefaultControllerCest
{
    public function it_send_a_get_request_to_the_index_page(FunctionalTester $I): void
    {
        $I->amOnPage('/');
        $I->seeResponseCodeIsSuccessful();
    }
}
