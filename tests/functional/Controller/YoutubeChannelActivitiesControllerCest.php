<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller;

use App\Componant\Constant\Status;
use App\Componant\Constant\YoutubeChannelStatus;
use App\Form\Type\PostButtonType;
use App\Tests\FunctionalTester;

class YoutubeChannelActivitiesControllerCest
{
    private const BASE_LOCATION = '/youtube_channels_activities';

    public function it_display_youtube_channels_activities_page(FunctionalTester $I): void
    {
        // Given
        $channel1 = $I->addYoutubeChannelInRepository(['id' => 'channel-id-1', 'name' => 'channel-1', 'status' => Status::OFFLINE]);
        $channel2 = $I->addYoutubeChannelInRepository(['id' => 'channel-id-2', 'name' => 'channel-2']);
        $channel3 = $I->addYoutubeChannelInRepository(['id' => 'channel-id-3', 'name' => 'channel-3']);
        $channel4 = $I->addYoutubeChannelInRepository(['id' => 'channel-id-4', 'name' => 'channel-4', 'status' => YoutubeChannelStatus::FAVORITE]);
        $id = 0;
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel1]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel1, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel2]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel2]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel2, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel3]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel3, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel4]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-'.++$id, 'name' => 'video-'.$id, 'channel' => $channel4, 'hiddenAt' => new \DateTime()]);

        // When
        $I->amOnPage(self::BASE_LOCATION);

        // Then
        $I->seeNumberOfElements('.youtube-channel', 2);
        $I->see('channel-2', '.youtube-channel');
        $I->see('channel-3', '.youtube-channel');
        $I->seeNumberOfElements('.youtube-video', 3);
        $I->see('video-3', '.youtube-video');
        $I->see('video-4', '.youtube-video');
        $I->see('video-6', '.youtube-video');
        $I->seeNumberOfElements('.youtube-hidden-channels li', 2);
        $I->see('video-5', '.youtube-hidden-channels li');
        $I->see('video-7', '.youtube-hidden-channels li');
    }

    public function it_clear_the_cache(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage(self::BASE_LOCATION);

        // When I click the clear the cache button
        $I->click('Rafraichir le cache');

        // Then
        $I->seeCurrentUrlEquals(self::BASE_LOCATION);
        $I->see('flash.cache.emptied');
    }

    public function it_hide_a_youtube_video(FunctionalTester $I): void
    {
        // Given
        $channel = $I->addYoutubeChannelInRepository();
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-5', 'name' => 'video-id-5', 'channel' => $channel,  'hiddenAt' => null]);
        $I->amOnPage(self::BASE_LOCATION);
        $I->see('video-id-5', '.youtube-video');

        // When I click on the hide button action
        $I->click('.youtube-video form[name="'.PostButtonType::BLOCK_PREFIX.'"] button');

        // Then I see the video is now hidden
        $I->seeCurrentUrlEquals(self::BASE_LOCATION);
        $I->dontSee('video-id-5', '.youtube-video');
        $I->see('video-id-5', '.youtube-hidden-channels li');
    }

    public function it_removes_hide_setting_from_a_youtube_video(FunctionalTester $I): void
    {
        // Given
        $channel = $I->addYoutubeChannelInRepository();
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-5', 'name' => 'video-id-5', 'channel' => $channel,  'hiddenAt' => new \DateTime()]);
        $I->amOnPage(self::BASE_LOCATION);
        $I->see('video-id-5', '.youtube-hidden-channels li');

        // When I click on the remove hide button action
        $I->click('.youtube-hidden-channels li form[name="'.PostButtonType::BLOCK_PREFIX.'"] button');

        // Then I see the video is now in the list of videos
        $I->seeCurrentUrlEquals(self::BASE_LOCATION);
        $I->dontSee('video-id-5', '.youtube-hidden-channels li');
        $I->see('video-id-5', '.youtube-video');
    }
}
