<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller;

use App\Service\ParameterService;
use App\Tests\FunctionalTester;

class ParametersControllerCest
{
    public function it_send_a_get_request_to_the_parameter_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();
        $I->amOnPage('/parameters');
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_post_request_to_the_parameter_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $I->sendPost('/parameters', [
            'parameter' => ParameterService::DEFAULT_PARAMETERS
        ]);
        $I->seeResponseCodeIsRedirection();
    }
}
