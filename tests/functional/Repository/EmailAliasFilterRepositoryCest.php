<?php

declare(strict_types=1);

namespace App\Tests\functional\Repository;

use App\Entity\EmailAliasFilter;
use App\Repository\EmailAliasFilterRepository;
use App\Tests\FunctionalTester;

final class EmailAliasFilterRepositoryCest
{
    private EmailAliasFilterRepository $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService('doctrine')->getRepository(EmailAliasFilter::class);
    }

    public function it_test_save_method(FunctionalTester $I): void
    {
        $I->expectTo('save an entity in the database');

        $aliasFilter = $I->createEmailAliasFilter(['email' => 'aliasFilter-email-field-9']);
        $this->repository->save($aliasFilter);

        $I->seeInRepository(EmailAliasFilter::class, ['email' => 'aliasFilter-email-field-9']);
    }
}
