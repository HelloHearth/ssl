<?php

declare(strict_types=1);

namespace App\Tests\functional\Repository;

use App\Entity\YoutubeVideo;
use App\Repository\YoutubeVideoRepository;
use App\Tests\FunctionalTester;

class YoutubeVideoRepositoryCest
{
    private YoutubeVideoRepository $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService('doctrine')->getRepository(YoutubeVideo::class);
    }

    public function it_test_save_list_method(FunctionalTester $I): void
    {
        $channel = $I->addYoutubeChannelInRepository();

        $youtubeVideo1 = $I->createYoutubeVideo(['id' => 'video-id-field-1', 'channel' => $channel]);
        $youtubeVideo2 = $I->createYoutubeVideo(['id' => 'video-id-field-5', 'channel' => $channel]);
        $youtubeVideo3 = $I->createYoutubeVideo(['id' => 'video-id-field-9', 'channel' => $channel]);

        $this->repository->saveList([$youtubeVideo1, $youtubeVideo2, $youtubeVideo3]);

        $I->assertEquals([$youtubeVideo1, $youtubeVideo2, $youtubeVideo3], $I->grabEntitiesFromRepository(YoutubeVideo::class));
    }

    public function it_find_hidden_videos(FunctionalTester $I): void
    {
        $channel = $I->addYoutubeChannelInRepository();

        $I->addYoutubeVideoInRepository(['id' => 'video-id-field-1', 'channel' => $channel]);
        $youtubeVideo2 = $I->addYoutubeVideoInRepository(['id' => 'video-id-field-5', 'channel' => $channel, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-field-9', 'channel' => $channel]);

        $result = $this->repository->findHidden();

        $I->assertEquals([$youtubeVideo2], $result);
    }

    public function it_find_hidden_videos_for_latest_days(FunctionalTester $I): void
    {
        $channel = $I->addYoutubeChannelInRepository();

        $I->addYoutubeVideoInRepository(['id' => 'video-id-field-1', 'channel' => $channel, 'hiddenAt' => new \DateTime('1998-07-12 22:45:00')]);
        $I->addYoutubeVideoInRepository(['id' => 'video-id-field-5', 'channel' => $channel, 'hiddenAt' => new \DateTime('2018-07-15 18:45:00')]);
        $youtubeVideo3 = $I->addYoutubeVideoInRepository(['id' => 'video-id-field-9', 'channel' => $channel, 'hiddenAt' => new \DateTime('-25 days')]);
        $youtubeVideo4 = $I->addYoutubeVideoInRepository(['id' => 'video-id-field-13', 'channel' => $channel, 'hiddenAt' => new \DateTime('-5 days')]);

        $result = $this->repository->findHidden(30);

        $I->assertEquals([$youtubeVideo4, $youtubeVideo3], $result);
    }

    public function it_count_not_hidden_by_channel(FunctionalTester $I): void
    {
        // Given
        $channel = $I->addYoutubeChannelInRepository(['id' => 'channel-1']);
        $channel2 = $I->addYoutubeChannelInRepository(['id' => 'channel-2']);
        $I->addYoutubeVideoInRepository(['id' => 'video-1', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-5', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-9', 'channel' => $channel, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-13', 'channel' => $channel, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-15', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-25', 'channel' => $channel2]);
        $I->addYoutubeVideoInRepository(['id' => 'video-27', 'channel' => $channel2, 'hiddenAt' => new \DateTime()]);

        // When
        $result = $this->repository->countNotHiddenByChannel($channel->getId());

        // Then
        $I->assertEquals(3, $result->getTotalNotHiddenVideos());
    }

    public function it_count_not_hidden_and_all_by_channel(FunctionalTester $I): void
    {
        // Given
        $channel = $I->addYoutubeChannelInRepository(['id' => 'channel-1']);
        $channel2 = $I->addYoutubeChannelInRepository(['id' => 'channel-2']);
        $I->addYoutubeVideoInRepository(['id' => 'video-1', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-5', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-9', 'channel' => $channel, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-13', 'channel' => $channel, 'hiddenAt' => new \DateTime()]);
        $I->addYoutubeVideoInRepository(['id' => 'video-15', 'channel' => $channel]);
        $I->addYoutubeVideoInRepository(['id' => 'video-25', 'channel' => $channel2]);
        $I->addYoutubeVideoInRepository(['id' => 'video-27', 'channel' => $channel2, 'hiddenAt' => new \DateTime()]);

        // When
        $result = $this->repository->countNotHiddenAndAllByChannel($channel->getId());

        // Then
        $I->assertEquals(3, $result->getTotalNotHiddenVideos());
        $I->assertEquals(5, $result->getTotalOfVideos());
    }
}
