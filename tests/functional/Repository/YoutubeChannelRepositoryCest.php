<?php

declare(strict_types=1);

namespace App\Tests\functional\Repository;

use App\Componant\Constant\Status;
use App\Componant\Constant\YoutubeChannelStatus;
use App\Entity\YoutubeChannel;
use App\Repository\YoutubeChannelRepository;
use App\Tests\FunctionalTester;

class YoutubeChannelRepositoryCest
{
    private YoutubeChannelRepository $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService('doctrine')->getRepository(YoutubeChannel::class);
    }

    public function it_find_all_online_channels_when_repository_is_empty(FunctionalTester $I): void
    {
        $I->assertSame([], $this->repository->findAllOnline());
    }

    public function it_find_all_online(FunctionalTester $I): void
    {
        // Given
        $entity1 = $I->addYoutubeChannelInRepository(['id' => 'channel-1', 'status' => Status::ONLINE]);
        $I->addYoutubeChannelInRepository(['id' => 'channel-5', 'status' => Status::OFFLINE]);
        $entity3 = $I->addYoutubeChannelInRepository(['id' => 'channel-9', 'status' => Status::ONLINE]);

        // When
        $result = $this->repository->findAllOnline();

        // Then
        $I->assertSame([$entity1, $entity3], $result);
    }

    public function it_find_favorites_channels_when_repository_is_empty(FunctionalTester $I): void
    {
        $I->assertSame([], $this->repository->findFavorites());
    }

    public function it_find_favorites_channels(FunctionalTester $I): void
    {
        // Given
        $entity1 = $I->addYoutubeChannelInRepository(['id' => 'channel-1', 'status' => YoutubeChannelStatus::FAVORITE]);
        $I->addYoutubeChannelInRepository(['id' => 'channel-5', 'status' => Status::OFFLINE]);
        $I->addYoutubeChannelInRepository(['id' => 'channel-9', 'status' => Status::ONLINE]);
        $entity4 = $I->addYoutubeChannelInRepository(['id' => 'channel-15', 'status' => YoutubeChannelStatus::FAVORITE]);

        // When
        $result = $this->repository->findFavorites();

        // Then
        $I->assertSame([$entity1, $entity4], $result);
    }

    public function it_find_not_offline_channels_when_repository_is_empty(FunctionalTester $I): void
    {
        $I->assertSame([], $this->repository->findNotOffline());
    }

    public function it_find_not_offline_channels(FunctionalTester $I): void
    {
        // Given
        $entity1 = $I->addYoutubeChannelInRepository(['id' => 'channel-1', 'status' => YoutubeChannelStatus::FAVORITE]);
        $I->addYoutubeChannelInRepository(['id' => 'channel-5', 'status' => Status::OFFLINE]);
        $entity3 = $I->addYoutubeChannelInRepository(['id' => 'channel-9', 'status' => Status::ONLINE]);
        $entity4 = $I->addYoutubeChannelInRepository(['id' => 'channel-15', 'status' => YoutubeChannelStatus::FAVORITE]);

        // When
        $result = $this->repository->findNotOffline();

        // Then
        $I->assertSame([$entity1, $entity4, $entity3], $result);
    }
}
