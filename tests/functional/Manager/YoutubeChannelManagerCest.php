<?php

declare(strict_types=1);

namespace App\Tests\functional\Manager;

use App\Componant\Constant\Status;
use App\Entity\YoutubeChannel;
use App\Manager\YoutubeChannelManager;
use App\Tests\FunctionalTester;

class YoutubeChannelManagerCest
{
    private YoutubeChannelManager $manager;

    public function _before(FunctionalTester $I): void
    {
        $this->manager = $I->grabService(YoutubeChannelManager::class);
    }

    public function it_save_entity(FunctionalTester $I): void
    {
        // Given
        $entity = $I->createYoutubeChannel(['id' => 'youtube-channel-Id-field-357']);
        $I->dontSeeInRepository(YoutubeChannel::class, ['id' => $entity->getId()]);

        // When
        $this->manager->save($entity);

        // Then
        $I->seeInRepository(YoutubeChannel::class, [
            'id' => 'youtube-channel-Id-field-357',
            'name' => 'youtubeChannel-name-field',
            'status' => Status::ONLINE,
        ]);
    }

    public function it_update_missing_appeared_at_with_published_at_of_first_video(FunctionalTester $I): void
    {
        // Given
        $channel1 = $I->createYoutubeChannel(['id' => 'channel-1']);
        $channel2 = $I->createYoutubeChannel(['id' => 'channel-2', 'appeared_at' => '-3 years']);
        $channel3 = $I->createYoutubeChannel(['id' => 'channel-3']);
        $videos = [];
        $videos[] = $I->createYoutubeVideo(['id' => 'video-1', 'channel' => $channel1, 'published_at' => new \DateTime('-5 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-2', 'channel' => $channel1, 'published_at' => new \DateTime('-30 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-3', 'channel' => $channel1, 'published_at' => new \DateTime('-55 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-4', 'channel' => $channel2, 'published_at' => new \DateTime('-5 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-5', 'channel' => $channel2, 'published_at' => new \DateTime('-30 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-6', 'channel' => $channel2, 'published_at' => new \DateTime('-55 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-7', 'channel' => $channel3, 'published_at' => new \DateTime('-25 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-8', 'channel' => $channel3, 'published_at' => new \DateTime('-30 minutes')]);
        $videos[] = $I->createYoutubeVideo(['id' => 'video-9', 'channel' => $channel3, 'published_at' => new \DateTime('-55 minutes')]);

        // When
        $this->manager->updateAppearedAt($videos);

        // Then
        $I->assertEquals($channel1->getAppearedAt()->getTimestamp(), $videos[0]->getPublishedAt()->getTimestamp());
        $I->assertEqualsWithDelta($channel2->getAppearedAt()->getTimestamp(), (new \DateTime('-3 years'))->getTimestamp(), 3);
        $I->assertEquals($channel3->getAppearedAt()->getTimestamp(), $videos[6]->getPublishedAt()->getTimestamp());
    }
}
