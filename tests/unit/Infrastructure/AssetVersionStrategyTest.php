<?php

declare(strict_types=1);

namespace App\Tests\unit\Infrastructure;

use App\Infrastructure\AssetVersionStrategy;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

final class AssetVersionStrategyTest extends Unit
{
    private const FILE_PATH = 'css/test-asset-version-strategy.css';
    private const FILE_MD5 = 'f5756d058e584f4c12c4a59ffadca875';

    private function createVersionStrategy(): AssetVersionStrategy
    {
        $parameters = new ParameterBag();
        $parameters->set('kernel.project_dir', getcwd().'/tests/_data');

        return new AssetVersionStrategy($parameters);
    }

    private function putContentsInAsset(string $content): void
    {
        \file_put_contents('./tests/_data/public/'.self::FILE_PATH, $content);
    }

    public function _before()
    {
        $this->putContentsInAsset('body{border:1px solid red;}');
    }

    /*
     * Proving that the md5 hash is not modified by time of the file, but only
     * by it's contents
     */
    public function test_get_version_method_return_md5_based_on_the_contents(): void
    {
        $versionStrategy = $this->createVersionStrategy();

        $this->assertSame(self::FILE_MD5, $versionStrategy->getVersion(self::FILE_PATH));

        $this->putContentsInAsset('body{border:1px solid green;}');
        $this->assertSame('3d39b9b7cb4337e9aa54d48f7625feb2', $versionStrategy->getVersion(self::FILE_PATH));

        $this->putContentsInAsset('body{border:1px solid red;}');
        $this->assertSame(self::FILE_MD5, $versionStrategy->getVersion(self::FILE_PATH));
    }

    public function test_apply_version_method(): void
    {
        $versionStrategy = $this->createVersionStrategy();

        $this->assertSame(self::FILE_PATH.'?'.self::FILE_MD5, $versionStrategy->applyVersion(self::FILE_PATH));
    }
}
