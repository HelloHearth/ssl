<?php

declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use Codeception\Test\Unit;

final class YoutubeVideoTest extends Unit
{
    public function test_entity_invocation(): void
    {
        $this->assertInstanceOf(YoutubeVideo::class, new YoutubeVideo());
    }

    public function test_entity_setter_and_getter_for_youtube_id(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setId('string-id');

        $this->assertSame('string-id', $YoutubeVideo->getId());
    }

    public function test_entity_setter_and_getter_for_created_at(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setCreatedAt($createdAt = new \DateTime());

        $this->assertSame($createdAt, $YoutubeVideo->getCreatedAt());
    }

    public function test_entity_setter_and_getter_for_description(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setDescription('string-description');

        $this->assertSame('string-description', $YoutubeVideo->getDescription());
    }

    public function test_entity_setter_and_getter_for_duration(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setDuration('string-duration');

        $this->assertSame('string-duration', $YoutubeVideo->getDuration());
    }

    public function test_entity_setter_and_getter_for_is_hidden(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setHiddenAt($hiddenAt = new \DateTime());

        $this->assertTrue($YoutubeVideo->isHidden());
        $this->assertSame($hiddenAt, $YoutubeVideo->getHiddenAt());

        $YoutubeVideo->unhide();

        $this->assertFalse($YoutubeVideo->isHidden());
        $this->assertNull($YoutubeVideo->getHiddenAt());

        $YoutubeVideo->hide();

        $this->assertTrue($YoutubeVideo->isHidden());
        $this->assertInstanceOf(\DateTime::class, $YoutubeVideo->getHiddenAt());
    }

    public function test_entity_setter_and_getter_for_name(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setName('string-name');

        $this->assertSame('string-name', $YoutubeVideo->getName());
    }

    public function test_entity_setter_and_getter_for_published_at(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setPublishedAt($publishedAt = new \DateTime('-1 day'));

        $this->assertSame($publishedAt, $YoutubeVideo->getPublishedAt());
    }

    public function test_entity_setter_and_getter_for_thumbnails(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setThumbnails($thumbnails = new \stdClass());

        $this->assertSame($thumbnails, $YoutubeVideo->getThumbnails());
    }

    public function test_entity_setter_and_getter_for_updated_at(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setUpdatedAt($updatedAt = new \DateTime('-3 day'));

        $this->assertSame($updatedAt, $YoutubeVideo->getUpdatedAt());
    }

    public function test_entity_setter_and_getter_for_url(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setUrl('string-url');

        $this->assertSame('string-url', $YoutubeVideo->getUrl());
    }

    public function test_entity_setter_and_getter_for_channel(): void
    {
        $YoutubeVideo = new YoutubeVideo();
        $YoutubeVideo->setChannel($channel = new YoutubeChannel());

        $this->assertSame($channel, $YoutubeVideo->getChannel());
    }
}
