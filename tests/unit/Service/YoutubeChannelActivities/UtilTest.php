<?php

declare(strict_types=1);

namespace App\Tests\unit\Service\YoutubeChannelActivities;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Service\HttpClient\HttpClientUtil;
use App\Service\YoutubeChannelActivities\Util;
use App\Service\YoutubeChannelActivities\ChannelModel;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

final class UtilTest extends Unit
{
    protected UnitTester $tester;

    public function test_get_channel_url_method_return_a_valid_url(): void
    {
        $expected = filter_var(Util::getChannelUrl('channelId'), FILTER_VALIDATE_URL);
        $this->assertNotFalse($expected);
    }

    public function test_get_channel_url_method_return_the_submitted_channel_id(): void
    {
        $channelId = '2346~a5&62+69-aa=32=e65°b9)a1/f0|250(a0[9d]93}4bf{cc"05#5';
        $this->assertStringContainsString($channelId, Util::getChannelUrl($channelId));
    }

    public function test_get_video_url_method_return_a_valid_url(): void
    {
        $expected = filter_var(Util::getVideoUrl('videoId'), FILTER_VALIDATE_URL);
        $this->assertNotFalse($expected);
    }

    public function test_get_video_url_method_return_the_submitted_video_id(): void
    {
        $videolId = '2346~a5&62+69-aa=32=e65°b9)a1/f0|250(a0[9d]93}4bf{cc"05#5';
        $this->assertStringContainsString($videolId, Util::getVideoUrl($videolId));
    }

    public function test_is_uploaded_video_activity_item_return_true_with_valid_data(): void
    {
        $activityItem = HttpClientUtil::transformContentResponseToObject('{"contentDetails":{"upload":{"videoId":"random-youtube-video-id"}}}');
        $this->assertTrue(Util::isUploadedVideoActivityItem($activityItem));
    }

    public function test_is_uploaded_video_activity_item_return_false_with_invalid_data(): void
    {
        $activityItem = HttpClientUtil::transformContentResponseToObject('{"contentDetails":{"upload":{"anotherId":"random-youtube-video-id"}}}');
        $this->assertFalse(Util::isUploadedVideoActivityItem($activityItem));
    }

    public function test_hydrate_youtube_video_with_content_response_method(): void
    {
        $contentResponse = $this->tester->getTransformedContentResponseForYoutubeVideoResources(['youtube-video-id-1']);
        $result = Util::hydrateYoutubeVideoWithContentResponse($contentResponse->items[0], new YoutubeChannel());

        $this->assertInstanceOf(YoutubeVideo::class, $result);
    }

    public function test_it_aggregate_videos_per_channel(): void
    {
        // Given
        $channel1 = $this->tester->createYoutubeChannel(['id' => 'channel-1']);
        $channel4 = $this->tester->createYoutubeChannel(['id' => 'channel-4']);
        $channel7 = $this->tester->createYoutubeChannel(['id' => 'channel-7']);
        $channel11 = $this->tester->createYoutubeChannel(['id' => 'channel-11']);
        $channel15 = $this->tester->createYoutubeChannel(['id' => 'channel-15']);

        $videos = [
            $video1 = $this->tester->createYoutubeVideo(['id' => 'id-1', 'channel' => $channel1]),
            $video2 = $this->tester->createYoutubeVideo(['id' => 'id-2', 'channel' => $channel1]),
            $video3 = $this->tester->createYoutubeVideo(['id' => 'id-3', 'channel' => $channel4]),
            $video5 = $this->tester->createYoutubeVideo(['id' => 'id-5', 'channel' => $channel7]),
            $video6 = $this->tester->createYoutubeVideo(['id' => 'id-6', 'channel' => $channel7]),
            $video7 = $this->tester->createYoutubeVideo(['id' => 'id-7', 'channel' => $channel11]),
            $video9 = $this->tester->createYoutubeVideo(['id' => 'id-9', 'channel' => $channel15]),
            $video10 = $this->tester->createYoutubeVideo(['id' => 'id-10', 'channel' => $channel1]),
            $video11 = $this->tester->createYoutubeVideo(['id' => 'id-11', 'channel' => $channel7]),
        ];

        // When
        $result = Util::aggregateVideosPerChannel($videos);

        // Then
        $this->assertContainsOnlyInstancesOf(ChannelModel::class, $result);
        $this->assertCount(5, $result);
        $this->assertEquals([$video1, $video2, $video10], $result['channel-1']->activities);
        $this->assertEquals([$video3], $result['channel-4']->activities);
        $this->assertEquals([$video5, $video6, $video11], $result['channel-7']->activities);
        $this->assertEquals([$video7], $result['channel-11']->activities);
        $this->assertEquals([$video9], $result['channel-15']->activities);
    }
}
