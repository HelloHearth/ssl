<?php

declare(strict_types=1);

namespace App\Tests\unit\Service\YoutubeChannelActivities;

use App\Service\YoutubeChannelActivities\Assert;
use InvalidArgumentException;
use RuntimeException;
use Codeception\Test\Unit;
use stdClass;

final class AssertTest extends Unit
{
    private function createItem(string $kind): stdClass
    {
        $item = new stdClass();
        $item->kind = $kind;
        return $item;
    }

    public function youTubeApiResponseProvider(): array
    {
        return [
            [$this->createItem('youtube#searchListResponse'), true],
            [$this->createItem('invalidApiResponse'), false],
        ];
    }

    /** @dataProvider youTubeApiResponseProvider */
    public function test_assert_is_youtube_api_response($item, $expected): void
    {
        // When
        $result = Assert::isYouTubeApiResponse($item, 'searchList');

        // Then
        $this->assertSame($expected, $result);
    }

    public function invalidItemProvider(): array
    {
        return [
            ['invalid object'],
            [new stdClass()],
        ];
    }

    /** @dataProvider invalidItemProvider */
    public function test_assert_is_youtube_api_response_with_invalid_item($invalidItem): void
    {
        // When
        $result = Assert::isYouTubeApiResponse($invalidItem, 'searchList');

        // Then
        $this->assertFalse($result);
    }

    public function test_array_contains_only_youtube_api_response(): void
    {
        // Given
        $item1 = $this->createItem('youtube#searchListResponse');
        $item2 = $this->createItem('youtube#searchListResponse');
        $item3 = $this->createItem('youtube#searchListResponse');

        // When
        Assert::arrayContainsOnlyYouTubeApiResponse([$item1, $item2, $item3], 'searchList');

        // Then
        $this->assertTrue(true);
    }

    public function test_expect_exception_when_array_not_contains_only_youtube_api_response(): void
    {
        // Expect
        $this->expectException(RuntimeException::class);

        // Given
        $item1 = $this->createItem('youtube#searchListResponse');
        $item2 = $this->createItem('invalidApiResponse');
        $item3 = $this->createItem('youtube#searchListResponse');

        // When
        Assert::arrayContainsOnlyYouTubeApiResponse([$item1, $item2, $item3], 'searchList');
    }

    public function test_when_array_of_ids_is_valid(): void
    {
        // Given
        $ids = \range(0, 49);

        // When
        Assert::arrayIsMaxYouTubeIds($ids);

        // Then
        $this->assertTrue(true);
    }

    public function test_expect_exception_when_array_of_ids_as_too_much_items(): void
    {
        // Expect
        $this->expectException(InvalidArgumentException::class);

        // Given
        $ids = \range(0, 50);

        // When
        Assert::arrayIsMaxYouTubeIds($ids);
    }
}
