<?php

declare(strict_types=1);

namespace App\Tests\unit\Service\AdministrationEmail;

use App\Repository\EmailAliasFilterRepository;
use App\Service\AdministrationEmail\CreateAliasFilterService;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

final class CreateAliasFilterServiceTest extends Unit
{
    private const FILTER_ALIAS_DOMAIN = '.test.filter@fake.tld';
    private const FILTER_ALIAS_FORWARD = 'default.forward@fake.tld';

    protected UnitTester $tester;

    private function createService(): CreateAliasFilterService
    {
        /** @var EmailAliasFilterRepository $repository */
        $repository = $this->makeEmpty(EmailAliasFilterRepository::class);

        return new CreateAliasFilterService(
            $repository,
            self::FILTER_ALIAS_DOMAIN,
            self::FILTER_ALIAS_FORWARD
        );
    }

    public function test_email_field_of_create_entity_method(): void
    {
        $Service = $this->createService();
        $aliasFilter = $Service->createEntity('filter-field', null, null);

        $this->assertSame('filter-field'.self::FILTER_ALIAS_DOMAIN, $aliasFilter->getEmail());
    }

    public function test_forward_field_of_create_entity_method(): void
    {
        $Service = $this->createService();
        $aliasFilter = $Service->createEntity('filter-field', null, null);

        $this->assertSame(self::FILTER_ALIAS_FORWARD, $aliasFilter->getForward());
    }

    public function test_comment_field_of_create_entity_method(): void
    {
        $Service = $this->createService();
        $aliasFilter = $Service->createEntity('filter-field', 'comment-field', null);

        $this->assertSame('comment-field', $aliasFilter->getComment());
    }

    public function test_from_url_field_of_create_entity_method(): void
    {
        $Service = $this->createService();
        $aliasFilter = $Service->createEntity('filter-field', null, 'from_url-field');

        $this->assertSame('from_url-field', $aliasFilter->getFromURL());
    }

    public function test_is_enabled_field_of_create_entity_method(): void
    {
        $Service = $this->createService();
        $aliasFilter = $Service->createEntity('filter-field', null, 'from_url-field');

        $this->assertTrue($aliasFilter->getIsEnabled());
    }
}
