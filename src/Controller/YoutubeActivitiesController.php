<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ClearCacheType;
use App\Manager\YoutubeChannelManager;
use App\Repository\YoutubeChannelRepository;
use App\Repository\YoutubeVideoRepository;
use App\Service\YoutubeChannelActivities\ChannelActivitiesService;
use App\Service\YoutubeChannelActivities\Util;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/youtube_channels_activities", name="app_youtube_channels_activities")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeActivitiesController extends AbstractController
{
    public const TWIG_DISPLAY_CHANNEL_NAME = 'twig_display_channel_name';

    private ChannelActivitiesService $service;
    private YoutubeChannelRepository $channelRepository;
    private YoutubeVideoRepository $videoRepository;
    private YoutubeChannelManager $channelManager;

    public function __construct(
        ChannelActivitiesService $service,
        YoutubeChannelRepository $channelRepository,
        YoutubeVideoRepository $videoRepository,
        YoutubeChannelManager $channelManager
    ) {
        $this->service = $service;
        $this->channelRepository = $channelRepository;
        $this->videoRepository = $videoRepository;
        $this->channelManager = $channelManager;
    }

    /**
     * @Route("", name="", methods={"GET"})
     */
    public function youtubeChannelsActivitiesAction(): Response
    {
        $channels = $this->channelRepository->findAllOnline();
        $this->service->fetchNewVideos($channels);

        $videoList = $this->videoRepository->findActiveOrderByPublishedAt();
        $this->channelManager->updateAppearedAt($videoList);

        $hiddenList = $this->videoRepository->findHidden(14);
        $clearCacheForm = $this->createForm(ClearCacheType::class, null, ['action' => $this->generateUrl('app_youtube_channels_activities_clear_cache')]);

        return $this->render('Pages/youtube-activities.html.twig', [
            'channels_activities' => Util::aggregateVideosPerChannel($videoList),
            'hidden_videos' => $hiddenList,
            'http_errors' => $this->service->getHttpErrors(),
            'published_after_days' => $this->service->getParameter('published_after_days'),
            'clear_cache_form' => $clearCacheForm->createView(),
            self::TWIG_DISPLAY_CHANNEL_NAME => true,
        ]);
    }

    /**
     * @Route("/clear_cache", name="_clear_cache", methods={"POST"})
     */
    public function clearCacheAction(Request $request): RedirectResponse
    {
        $form = $this->createForm(ClearCacheType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->clearCache();
            $this->addFlash('success', 'flash.cache.emptied');
        }

        return $this->redirectToRoute('app_youtube_channels_activities');
    }
}
