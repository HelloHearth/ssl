<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ParameterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ParametersController extends AbstractController
{
    /**
     * @Route("/parameters", name="app_parameters", methods={"GET", "POST"})
     */
    public function index(Request $request, ParameterService $service): Response
    {
        $parameters = $service->getParameters();

        if ('POST' === $request->getMethod()) {

            $service->updateAndSaveParameters($request->request->get('parameter'));

            return $this->redirectToRoute($request->get('_route'));
        }

        return $this->render('Pages/parameters.html.twig', [
            'parameters' => $parameters
        ]);
    }
}
