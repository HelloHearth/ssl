<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractAppController extends AbstractController
{
    protected function getFormErrorMessage(FormInterface $form): ?string
    {
        $errors = $form->getErrors();
        if (! isset($errors[0]) || ! $errors[0] instanceof FormError) {
            return null;
        }

        return $errors[0]->getMessage();
    }

    protected function addFlashOnFormError(FormInterface $form): void
    {
        if (! $form->isValid()) {
            $errorMessage = $this->getFormErrorMessage($form);
            $this->addFlash('danger', \is_string($errorMessage) ? $errorMessage : 'flash.common.unknown_error');
        }
    }

    protected function redirectToReferer(Request $request, string $fallback = null): RedirectResponse
    {
        if ($referer = $request->headers->get('referer')) {
            return $this->redirect($referer);
        }

        if (\is_string($fallback)) {
            return $this->redirectToRoute($fallback);
        }

        return $this->redirectToRoute('app_homepage');
    }
}
