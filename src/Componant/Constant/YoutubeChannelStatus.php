<?php

declare(strict_types=1);

namespace App\Componant\Constant;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelStatus extends Status
{
    const FAVORITE = 'favorite';

    /**
     * {@inheritDoc}
     *
     * @return string[]
     */
    public static function getStatutes(): array
    {
        return \array_merge(parent::getStatutes(), [
            self::FAVORITE => self::FAVORITE,
        ]);
    }
}
