<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\YoutubeChannelActivities\ChannelModel;
use Swift_Mailer;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MailerService
{
    private Swift_Mailer $mailer;
    private Environment $twig;
    private TranslatorInterface $translator;
    private string $adminEmail;

    public function __construct(Swift_Mailer $mailer, Environment $twig, TranslatorInterface $translator, string $adminEmail)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->adminEmail = $adminEmail;
    }

    public function notifyYouTubeChannelHasUploadedVideos(ChannelModel $channel): void
    {
        $message = new \Swift_Message();

        $count = \count($channel->activities);
        $subject = $this->translator->trans('notification.youtube_activity.uploaded_video', [
            'channel_name' => $channel->title,
            'count' => $count,
        ], 'notification');

        $message
            ->setFrom('no-reply@localhost')
            ->setTo($this->adminEmail)
            ->setSubject($subject)
            ->setBody(
                $this->twig->render('Mail/youtube-activities.html.twig', [
                    'title' => $subject,
                    'channel' => $channel,
                    'count' => $count,
                ]),
                'text/html'
            )
        ;

        $this->mailer->send($message);
    }
}
