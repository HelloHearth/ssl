<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Parameter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ParameterService
{
    // Parameters enumerator
    const DEFAULT_PARAMETERS = [
        'youtube_activities_published_after_days' => '7'
    ];

    private EntityManagerInterface $em;
    /** @var Parameter[] $parameters */
    private array $parameters;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->loadParameters();
    }

    private function loadParameters(): void
    {
        $this->parameters = $this->em->getRepository(Parameter::class)->findAll();

        // Remove stored parameter if it's not in the default enumerator
        foreach ($this->parameters as $key => $parameter) {
            if (! \array_key_exists($parameter->getId(), self::DEFAULT_PARAMETERS)) {
                unset($this->parameters[$key]);
                $this->em->remove($parameter);
            }
        }

        // Load default parameters if not found in the repository
        foreach (self::DEFAULT_PARAMETERS as $key => $default) {

            if (! $this->has($key)) {

                $parameter = new Parameter();

                $parameter->setId($key);
                $parameter->setValue((string) $default);

                $this->em->persist($parameter);

                $this->parameters[] = $parameter;
            }
        }
    }

    public function has(string $key): bool
    {
        foreach ($this->parameters as $parameter) {
            if ($key === $parameter->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Parameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function updateAndSaveParameters(array $data): void
    {
        foreach ($this->parameters as $parameter) {
            if (isset($data[$parameter->getId()])) {
                $parameter->setValue($data[$parameter->getId()]);
            }
        }
        $this->em->flush();
    }
}
