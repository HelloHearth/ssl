<?php

declare(strict_types=1);

namespace App\Service\HttpClient;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class HttpClientUtil
{
    public static function transformContentResponseToObject(string $contentResponse): object
    {
        return \json_decode($contentResponse);
    }

    /**
     * @return object[]
     */
    public static function transformResponsesContentsToObjects(array $contentsResponses): array
    {
        $contents = [];

        foreach ($contentsResponses as $key => $content) {
            $contents[$key] = self::transformContentResponseToObject($content);
        }

        return $contents;
    }
}
