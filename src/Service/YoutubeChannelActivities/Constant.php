<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class Constant
{
    public const YOUTUBE_MAX_RESULT = 50;
}
