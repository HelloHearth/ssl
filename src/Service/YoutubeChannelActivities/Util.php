<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Util\DateUtil;

/**
 * YouTube API documentation for Activities::list
 * https://developers.google.com/youtube/v3/docs/activities/list
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class Util
{
    public static function getChannelUrl(string $channelId): string
    {
        return 'https://www.youtube.com/channel/'.$channelId.'/videos';
    }

    public static function getVideoUrl(string $videoId): string
    {
        return 'https://www.youtube.com/watch?v='.$videoId;
    }

    public static function isUploadedVideoActivityItem(object $activityItem): bool
    {
        return isset($activityItem->contentDetails->upload->videoId);
    }

    public static function getUploadedVideoId(object $activityItem): string
    {
        return $activityItem->contentDetails->upload->videoId;
    }

    public static function hydrateYoutubeVideoWithContentResponse(object $item, YoutubeChannel $youtubeChannel): YoutubeVideo
    {
        $youtubeVideo = new YoutubeVideo();

        if ($item->kind === 'youtube#video') {
            $youtubeVideo->setId($item->id);
            if (\is_string($item->contentDetails->duration)) {
                $youtubeVideo->setDuration(DateUtil::formatDuration($item->contentDetails->duration));
            }
        } elseif (isset($item->snippet->resourceId->kind) && $item->snippet->resourceId->kind === 'youtube#video') {
            $youtubeVideo->setId($item->snippet->resourceId->videoId);
        }

        $youtubeVideo->setChannel($youtubeChannel);
        $youtubeVideo->setUrl(self::getVideoUrl($youtubeVideo->getId()));
        $youtubeVideo->setDescription($item->snippet->description);
        $youtubeVideo->setName($item->snippet->title);
        $youtubeVideo->setThumbnails($item->snippet->thumbnails);
        $youtubeVideo->setPublishedAt(new \DateTime($item->snippet->publishedAt));
        $youtubeVideo->unhide();

        return $youtubeVideo;
    }

    /**
     * @param YoutubeVideo[] $videos
     *
     * @return ChannelModel[]
     */
    public static function aggregateVideosPerChannel(array $videos): array
    {
        $channels = [];

        foreach ($videos as $video) {

            $channelId = $video->getChannel()->getId();

            if (isset($channels[$channelId])) {
                $channel = $channels[$channelId];
            } else {
                $channel = ChannelModel::fromEntity($video->getChannel());
                $channels[$channelId] = $channel;
            }

            $channel->addVideoItem($video);
        }

        // Sort channels by apparition date
        \uasort($channels, function (ChannelModel $a, ChannelModel $b): int {
            if ($a->apparitionAt > $b->apparitionAt) {
                return -1;
            }
            if ($a->apparitionAt < $b->apparitionAt) {
                return 1;
            }
            return 0;
        });

        return $channels;
    }
}
