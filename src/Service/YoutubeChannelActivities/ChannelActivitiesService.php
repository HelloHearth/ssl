<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use App\Entity\Parameter;
use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Repository\ParameterRepository;
use App\Repository\YoutubeChannelRepository;
use App\Repository\YoutubeVideoRepository;
use App\Service\HttpClient\HttpClient;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Psr\Cache\CacheItemPoolInterface;

/**
 * YouTube API documentation for Activities::list
 * https://developers.google.com/youtube/v3/docs/activities/list
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelActivitiesService
{
    public const CACHE_ITEM_PREFIX = 'youtube_activity_channel_id_';

    private HttpClient $httpClient;
    private YoutubeChannelRepository $channelRepository;
    private YoutubeVideoRepository $videoRepository;
    private ParameterRepository $parameterRepository;
    private CacheItemPoolInterface $cacheItemPool;
    private array $parameters;

    public function __construct(
        HttpClient $httpClient,
        YoutubeChannelRepository $channelRepository,
        YoutubeVideoRepository $videoRepository,
        ParameterRepository $parameterRepository,
        CacheItemPoolInterface $cacheItemPool,
        array $youtubeParameters
    ) {
        $this->httpClient = $httpClient;
        $this->channelRepository = $channelRepository;
        $this->videoRepository = $videoRepository;
        $this->parameterRepository = $parameterRepository;
        $this->cacheItemPool = $cacheItemPool;
        $this->parameters = $youtubeParameters;

        // Setting custom parameter from repository
        $parameter = $this->parameterRepository->find('youtube_activities_published_after_days');
        if ($parameter instanceof Parameter) {
            $this->parameters['published_after_days'] = $parameter->getValue();
        }
    }

    /**
     * @return mixed|null
     */
    public function getParameter(string $key)
    {
        if (isset($this->parameters[$key])) {
            return $this->parameters[$key];
        }
        return null;
    }

    public function getHttpErrors(): array
    {
        return $this->httpClient->getErrorMessages();
    }

    /**
     * Get a list of recently uploaded videos
     *
     * @param YoutubeChannel[] $channels
     *
     * @return YoutubeVideo[]
     *
     * @throws Exception
     */
    public function fetchNewVideos(array $channels): array
    {
        $youtubeActivityList = $this->fetchChannelsActivities($channels);

        if (empty($youtubeActivityList)) {
            return [];
        }

        Assert::arrayContainsOnlyYouTubeApiResponse($youtubeActivityList, 'activityList');
        $youtubeActivityIds = $this->getUploadedVideosIds($youtubeActivityList);

        $storedVideos = $this->videoRepository->findBy(['id' => $youtubeActivityIds]);
        $newVideosIds = $this->extractNewVideosIds($youtubeActivityIds, $storedVideos);

        $chunkedIds = \array_chunk($newVideosIds, Constant::YOUTUBE_MAX_RESULT);
        $newVideos = [];
        foreach ($chunkedIds as $ids) {
            $newVideos = \array_merge($newVideos, $this->getAndSaveNewVideos($ids));
        }

        return $newVideos;
    }

    public function clearCache(): void
    {
        $this->cacheItemPool->clear();
    }

    /**
     * Ask the YouTube API for activities of channels.
     *
     * @param YoutubeChannel[] $channels
     *
     * @throws Exception
     */
    private function fetchChannelsActivities(array $channels): array
    {
        $responses = [];
        $publishedAfter = new DateTimeImmutable('-'.$this->parameters['published_after_days'].' days midnight');

        $parameters = [
            'key' => $this->parameters['developer_key'],
            'part' => 'id,contentDetails',
            'publishedAfter' => $publishedAfter->format(DateTimeInterface::ATOM),
            'maxResults' => Constant::YOUTUBE_MAX_RESULT,
        ];

        foreach ($channels as $channel) {

            $cacheItem = $this->cacheItemPool->getItem(self::CACHE_ITEM_PREFIX.$channel->getId());

            if ($cacheItem->isHit()) {
                continue;
            }

            $cacheItem->set(false);
            $cacheItem->expiresAfter(3600);
            $this->cacheItemPool->save($cacheItem);

            $parameters['channel_id'] = $channel->getId();
            $responses[$channel->getId()] = $this->httpClient->request('GET', 'activities?'.\http_build_query($parameters));
        }

        return $this->httpClient->getResponsesContents($responses, true);
    }

    /**
     * Ask the YouTube API for video resources
     *
     * @param string[] $ids
     *
     * @return array
     */
    private function fetchVideoResources(array $ids): array
    {
        // Avoid invalid request parameter from YouTube api
        Assert::arrayIsMaxYouTubeIds($ids);

        $query = http_build_query([
            'key' => $this->parameters['developer_key'],
            'id' => join(',', $ids),
            'part' => 'snippet,contentDetails'
        ]);

        $response = $this->httpClient->request('GET', 'videos?'.$query);

        return $this->httpClient->getResponsesContents([$response], true);
    }

    /**
     * Extract from a list of HTTP responses contents, all uploaded
     * videos activities in YouTube.
     *
     * @param object[] $youtubeActivityList
     *
     * @return int[]
     */
    private function getUploadedVideosIds(array $youtubeActivityList): array
    {
        $ids = [];

        foreach ($youtubeActivityList as $content) {
            foreach ($content->items as $item) {
                if (Util::isUploadedVideoActivityItem($item)) {
                    $ids[] = Util::getUploadedVideoId($item);
                }
            }
        }

        return $ids;
    }

    /**
     * Compare a given list of videos ids, with a list videos stored in
     * the database, and return the list of video ids which resources require to
     * fetch.
     *
     * @param int[] $youTubeActivityIds
     * @param YoutubeVideo[] $storedVideos
     *
     * @return int[]
     */
    private function extractNewVideosIds(array $youTubeActivityIds, array $storedVideos): array
    {
        $storedVideosId = [];
        foreach ($storedVideos as $storedVideo) {
            $storedVideosId[] = $storedVideo->getId();
        }

        return array_diff($youTubeActivityIds, $storedVideosId);
    }

    /**
     * @param int[] $newVideosIds
     *
     * @return YoutubeVideo[]
     */
    private function getAndSaveNewVideos(array $newVideosIds): array
    {
        if (empty($newVideosIds)) {
            return [];
        }

        $videoResources = $this->fetchVideoResources($newVideosIds);
        Assert::arrayContainsOnlyYouTubeApiResponse($videoResources, 'videoList');

        if (! isset($videoResources[0])) {
            return [];
        }

        foreach ($videoResources[0]->items as $videoResource) {
            $channel = $this->channelRepository->find($videoResource->snippet->channelId);
            $newVideos[] = Util::hydrateYoutubeVideoWithContentResponse($videoResource, $channel);
        }

        $this->videoRepository->saveList($newVideos);

        return $newVideos;
    }
}
