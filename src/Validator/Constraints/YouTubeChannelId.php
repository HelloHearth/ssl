<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YouTubeChannelId extends Constraint
{
    public string $message = 'validation.youtube_channel_id.message';
}
