<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity()
 * @UniqueEntity("id", message="La chaine Youtube est déjà inscrite dans la base de donnée")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Parameter
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=4000)
     */
    private $value;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }
}
