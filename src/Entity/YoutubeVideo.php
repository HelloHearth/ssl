<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\YoutubeVideoRepository")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class YoutubeVideo
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @var YoutubeChannel
     *
     * @ORM\ManyToOne(targetEntity="YoutubeChannel")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     */
    private $channel;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    private $duration;

    /**
     * @var object
     *
     * @ORM\Column(type="object", length=4096)
     */
    private $thumbnails;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $hiddenAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getChannel(): ?YoutubeChannel
    {
        return $this->channel;
    }

    public function setChannel(YoutubeChannel $channel): void
    {
        $this->channel = $channel;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): void
    {
        $this->duration = $duration;
    }

    public function getThumbnails(): ?object
    {
        return $this->thumbnails;
    }

    public function setThumbnails(object $thumbnails): void
    {
        $this->thumbnails = $thumbnails;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getPublishedAt(): ?\DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    public function getHiddenAt(): ?\DateTime
    {
        return $this->hiddenAt;
    }

    public function setHiddenAt(?\DateTime $isHidden): void
    {
        $this->hiddenAt = $isHidden;
    }

    public function isHidden(): bool
    {
        return $this->hiddenAt instanceof \DateTime;
    }

    public function hide(): void
    {
        $this->hiddenAt = new \DateTime();
    }

    public function unhide(): void
    {
        $this->hiddenAt = null;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
