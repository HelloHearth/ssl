<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\YoutubeChannel;
use App\Form\Type\YoutubeChannelStatusType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelEditType extends AbstractType
{
    const BLOCK_PREFIX = 'edit_youtube_channel';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom de la chaine',
                'required' => true,
            ])
            ->add('status', YoutubeChannelStatusType::class)
            ->add('submit', SubmitType::class, ['label' => 'Modifier'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => YoutubeChannel::class,
            'validation_groups' => ['EDIT'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
