<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class PostButtonHideVideoType extends AbstractType
{
    public const DISPLAY_CHANNEL_NAME = 'display_channel_name';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::DISPLAY_CHANNEL_NAME, HiddenType::class, [
                'data' => $options[self::DISPLAY_CHANNEL_NAME],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            self::DISPLAY_CHANNEL_NAME => false,
        ]);

        $resolver->setAllowedValues(self::DISPLAY_CHANNEL_NAME, [true, false]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return PostButtonType::class;
    }
}
