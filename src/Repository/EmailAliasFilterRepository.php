<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\EmailAliasFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailAliasFilter|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailAliasFilter|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailAliasFilter[]    findAll()
 * @method EmailAliasFilter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmailAliasFilterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailAliasFilter::class);
    }

    public function save(EmailAliasFilter $aliasFilter): int
    {
        $em = $this->getEntityManager();

        $em->persist($aliasFilter);
        $em->flush();

        return $aliasFilter->getId();
    }
}
