#!/bin/bash

source "./.env.deploy.local" || exit 2

if [ "$APP_ENV" == "prod" ]; then
    echo "Bad environement for using this script"
    exit 2
fi

echo 'Run command: git push'
git push

echo
echo "Connexion ssh à "$APP_DEPLOY_USER@$APP_DEPLOY_HOST"/"$APP_DEPLOY_PATH
echo

ssh -t $APP_DEPLOY_USER@$APP_DEPLOY_HOST "

    cd $APP_DEPLOY_PATH || exit 1

    echo Run command: git pull --no-edit
    git pull --no-edit

    echo
    echo Run command: composer install
    composer install --no-dev --optimize-autoloader
"
